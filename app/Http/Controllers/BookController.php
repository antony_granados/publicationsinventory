<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Repositories\BookRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BookController extends InfyOmBaseController
{
    /** @var  bookRepository */
    private $bookRepository;

    public function __construct(bookRepository $bookRepo)
    {
        $this->bookRepository = $bookRepo;
    }

    /**
     * Display a listing of the books.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->bookRepository->pushCriteria(new RequestCriteria($request));
        $books = $this->bookRepository->all();

        return view('book.index')
            ->with('books', $books);
    }

    /**
     * Show the form for creating a new books.
     *
     * @return Response
     */
    public function create()
    {
        return view('book.create');
    }

    /**
     * Store a newly created book in storage.
     *
     * @param CreateBookRequest $request
     *
     * @return Response
     */
    public function store(CreateBookRequest $request)
    {
        $input = $request->all();

        $book = $this->bookRepository->create($input);

        Flash::success('Book saved successfully.');

        return redirect(route('book.index'));
    }

    /**
     * Display the specified book.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $book = $this->bookRepository->findWithoutFail($id);

        if (empty($book)) {
            Flash::error('book not found');

            return redirect(route('book.index'));
        }

        return view('book.show')->with('book', $book);
    }

    /**
     * Show the form for editing the specified books.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $book = $this->bookRepository->findWithoutFail($id);

        if (empty($book)) {
            Flash::error('book not found');

            return redirect(route('book.index'));
        }

        return view('book.edit')->with('book', $books);
    }

    /**
     * Update the specified book in storage.
     *
     * @param  int              $id
     * @param UpdateBookRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBookRequest $request)
    {
        $book = $this->bookRepository->findWithoutFail($id);

        if (empty($books)) {
            Flash::error('book not found');

            return redirect(route('book.index'));
        }

        $book = $this->bookRepository->update($request->all(), $id);

        Flash::success('book updated successfully.');

        return redirect(route('book.index'));
    }

    /**
     * Remove the specified book from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $book = $this->bookRepository->findWithoutFail($id);

        if (empty($book)) {
            Flash::error('book not found');

            return redirect(route('books.index'));
        }

        $this->bookRepository->delete($id);

        Flash::success('book deleted successfully.');

        return redirect(route('books.index'));
    }
}
