@extends('layouts.app')

@section('content')
    @include('book.show_fields')

    <div class="form-group">
           <a href="{!! route('books.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
