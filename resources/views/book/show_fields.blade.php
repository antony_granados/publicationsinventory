<!-- Id Field -->
<div class="form-group">
    {!! Form::label('Id', 'Id:') !!}
    <p>{!! $book->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('Title', 'Título:') !!}
    <p>{!! $book->title !!}</p>
</div>

<!-- Author Field -->
<div class="form-group">
    {!! Form::label('Author', 'Autor:') !!}
    <p>{!! $book->author !!}</p>
</div>

<!-- Year Field -->
<div class="form-group">
    {!! Form::label('Year', 'Año:') !!}
    <p>{!! $book->year !!}</p>
</div>

<!-- Units Field -->
<div class="form-group">
    {!! Form::label('Units', 'Unidades:') !!}
    <p>{!! $book->units !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('Price', 'Precio:') !!}
    <p>{!! $book->price !!}</p>
</div>

<!-- Timemark Field -->
<div class="form-group">
    {!! Form::label('timeMark', 'Marca temporal:') !!}
    <p>{!! $book->timeMark !!}</p>
</div>

