@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Book</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($book, ['route' => ['book.update', $book->id], 'method' => 'patch']) !!}

            @include('book.fields')

            {!! Form::close() !!}
        </div>
@endsection
